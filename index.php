<?php require_once "./code.php"; ?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP Basics and Selection Control Structures Activity</title>
</head>
<body>

	<h1>Full Address</h1>
	<p> <?php echo getFullAddress('3F Enzo Bldg.', 'Buendia Avenue, Makati City', 'Metro Manila', 'Philippines'); ?></p>

	<p> <?php echo getFullAddress('2770 San Rafael St.', ' Sucat, Paranaque City', 'Metro Manila', 'Philippines'); ?></p>


	<h2>Letter-Based Grading</h2>
	<p><?php 

		
		$grade1 = 87;
		$letterGrade1 = getLetterGrade($grade1);
		echo $grade1 . " is equivalent to " . $letterGrade1 . "<br>";

	
		$grade2 = 94;
		$letterGrade2 = getLetterGrade($grade2);
		echo $grade2 . " is equivalent to " . $letterGrade2 . "<br>";

	
		$grade3 = 74;
		$letterGrade3 = getLetterGrade($grade3);
		echo $grade3 . " is equivalent to " . $letterGrade3 . "<br>";
		?></p>
</body>
</html>